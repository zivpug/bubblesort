export class SortService {

    // based on bubble sort sample from - https://medium.com/javascript-algorithms/javascript-algorithms-bubble-sort-3d27f285c3b2

    /*
    * sort function will return a list of arrays, each with an index indicator for the changed item
    * - all the steps to be animated
    * */
    sort = (arr) => {
        let sortedArr = (!Array.isArray(arr)) ? arr.split(',').map(Number) : arr.map(Number);
        let len = sortedArr.length;
        let swapped;
        let animateArr = [];
        do {
            swapped = false;
            for (let i = 0; i < len; i++) {
                if (sortedArr[i] > sortedArr[i + 1]) {
                    sortedArr = this.swap(sortedArr, i);
                    animateArr.push({sortedArr: sortedArr.map(Number), index:i});
                    swapped = true;
                }
            }

        } while (swapped);

        return {animateArr};
    };

    swap = (arr, i) => {
        [arr[i], arr[i + 1]] = [arr[i + 1], arr[i]];
        return arr;
    };

};
