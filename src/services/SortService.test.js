import {SortService} from './SortService';

// initiate the service
const sortService = new SortService();

// mock data for testing
const testArr = [12, 6, 10, 1, 55, 2, 9, 24, 32];
const sorted = [1, 2, 6, 9, 10, 12, 24, 32, 55];

// run the sort function
const res = sortService.sort(testArr);

it('returnes an animations list array', () => {
    expect(res.animateArr).toHaveLength(14);
});


it('returns correct objects for animations steps', () => {
    expect(res.animateArr[0]).toHaveProperty('sortedArr');
    expect(res.animateArr[0]).toHaveProperty('index');
});


it('returnes a correctly sorted array in the last animation step', () => {
    expect(res.animateArr[12].sortedArr).not.toEqual(sorted);
    expect(res.animateArr[13].sortedArr).toEqual(sorted);
});

