import React from 'react';

import {SortService} from './services/SortService';

import {SingleFieldComponent} from './components/SingleFieldComponent';
import {SorterComponent} from './components/SorterComponent';
import './App.css';

// sample array - just for initial run
const sampArr = [12, 6, 10, 1, 55, 2, 9, 24, 32];

export default class App extends React.Component {
    // init the sorting service
    sort = new SortService();

    constructor() {
        super();

        this.state = {
            arr: [],
            animateArr: []
        }
    }

    sortArr = (arr) => {
        const {animateArr} = this.sort.sort(arr);
        this.setState({arr, animateArr});
    };

    componentWillMount() {
        this.sortArr(sampArr);
    }

    render() {
        return (
            <div className="App">
                <h2>Bubblesort</h2>

                <SingleFieldComponent
                    action={this.sortArr}
                    placeholder={'Insert comma seperated values to sort'}
                    btClass={'bigBtn'}
                    fieldClass={'bigField'}
                    btText={'Sort Us!'}
                />


                <SorterComponent
                    animateArr={this.state.animateArr}/>
            </div>
        )
    }
}
