import React from "react";

/*
* Basic field and submit button component
* very useful for any app
* */

export const SingleFieldComponent = (props) => {
    const {action, btText='Submit', btClass, fieldClass, placeholder} = props;

    // call the action callback function when form submitted
    const handleSubmit = (event) => {
        event.preventDefault();
        const val = event.currentTarget.parentElement[0].value;
        action(val);
    };

    return <form onSubmit={handleSubmit}>
        <input
            className={fieldClass}
            type={'text'}
            placeholder={placeholder}
        />


        <button
            onClick={handleSubmit}
            className={btClass}>
            {btText}
        </button>
    </form>
};
