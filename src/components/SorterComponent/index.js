import React from "react";
import {AnimateKeyframes, AnimateGroup} from 'react-simple-animate';

import './style.css';

export class SorterComponent extends React.Component {
    // animation interval - in seconds
    interval = 2;

    constructor(props) {
        super(props);
        /*
        * animateArr - will hold list of all animation stages
        * selectedArr - the current running stage
        * */
        this.state = {
            animateArr: this.props.animateArr,
            selectedArr: [],
            phase: 1
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({animateArr: nextProps.animateArr});
        if (this.state.animateArr) {
            // start animating
            this.aminate();
        }
    }

    componentWillMount() {
        if (this.props.animateArr) {
            // start animating
            this.aminate();
        }
    }

    render() {
        return (
            <div className={'sorterContainer'}>
                <ul className={'listContainer'}>
                    {this.showArr()}
                </ul>
            </div>
        )
    }

    // main list building function
    showArr = () => {
        const {sortedArr, index} = this.state.selectedArr;
        let re;
        // phase 1 will reset css animations
        if (this.state.phase === 2 && sortedArr && sortedArr.length) {
            // will set animation block if the item is the selected item, or the one next to it
            re = <AnimateGroup play>
                {/* index value will set the selected item to animate */}
                {sortedArr.map((item, i) =>
                    ((i === index + 1 || i === index + 2) && index < sortedArr.length - 2) ?
                        this.animBlock(item, i, index) :
                        this.numberDiv(item, i))}
            </AnimateGroup>;
        } else if (sortedArr) {
            re = sortedArr.map((item, i) => this.numberDiv(item, i));
            this.setPhase(2);
        }

        return re;
    };

    // set phase - mainly for animations reset
    setPhase = (phase) => {
        setTimeout(() => this.setState({phase}), this.interval);
    };

    // return simple div block if item is not the selected item
    numberDiv = (item, i) => {
        return <li className={'item'} key={'k' + i}>{item}</li>
    };

    // will return animated block for the selected item/s
    animBlock = (item, i, index) => {
        // various animation types - for 2 types of items
        const animationDir =
            [
                [

                    "transform: translateY(0px)",
                    "transform: translateY(-40px)",
                    "transform: translate(90px, -40px)",
                    "transform: translate(90px, 0)"
                ],
                [
                    "transform: translateY(0px)",
                    "transform: translateY(-40px)",
                    "transform: translate(-90px, -40px)",
                    "transform: translate(-90px, 0)"
                ]
            ];
        // determine which animation to use
        const dir = (i === index + 1) ? 0 : 1;
        // return the animation object
        return <AnimateKeyframes
            play
            key={'k' + i}
            iterationCount={String(1 + dir)}
            duration={this.interval}
            keyframes={animationDir[dir]}
        >
            <div className={'item selectredItem'}>{item}</div>
        </AnimateKeyframes>
    };


    /*
    * Engine functions
    * to run the animations in sequence
    * */
    aminate = (i = 0) => {
        const L = this.state.animateArr.length;
        if (i < L) {
            const selectedArr = this.state.animateArr[i];
            setTimeout(() => this.step(selectedArr, i), this.interval * 1000);
        }
    };

    step = (selectedArr = [], i) => {
        this.setState({selectedArr, phase: 1}, this.aminate(i + 1))
    };
};


